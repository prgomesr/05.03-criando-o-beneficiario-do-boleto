package br.com.ozeano.curso.api.bb.domain.service.file;

import java.text.ParseException;

import javax.swing.text.MaskFormatter;

import br.com.caelum.stella.boleto.Beneficiario;
import br.com.caelum.stella.boleto.Datas;
import br.com.caelum.stella.boleto.Endereco;
import br.com.ozeano.curso.api.bb.domain.model.Fatura;
import br.com.ozeano.curso.api.bb.infra.model.input.CobrancaInput;

public interface GeradorDeBoletoService {

	public byte[] gerar(Fatura fatura, CobrancaInput cobranca);
	
	
	default Datas criarDatas(Fatura fatura) {
		
		var vencimento = fatura.getDataVencimento();
		var criadoEm = fatura.getCriadoEm();
		var atualizadoEm = fatura.getAtualizadoEm();
		
		var datas = Datas.novasDatas()
				.comDocumento(criadoEm.getDayOfMonth(), criadoEm.getMonthValue(), criadoEm.getYear())
				.comProcessamento(atualizadoEm.getDayOfMonth(), atualizadoEm.getMonthValue(), atualizadoEm.getYear())
				.comVencimento(vencimento.getDayOfMonth(), vencimento.getMonthValue(), vencimento.getYear());
		
		return datas;
	}
	
	default Beneficiario criarBeneficiario(Fatura fatura) {
		
		var empresa = fatura.getConta().getEmpresa();
		var conta = fatura.getConta();
		var endereco = Endereco.novoEndereco()
				.comLogradouro(empresa.getEndereco().getLogradouro().concat(", ").concat(empresa.getEndereco().getNumero()))
				.comBairro(empresa.getEndereco().getBairro())
				.comCep(insereMascaraAoRetornarDocumento(empresa.getEndereco().getCep()))
				.comCidade(empresa.getEndereco().getCidade())
				.comUf(empresa.getEndereco().getUf());
		
		var beneficiario = Beneficiario.novoBeneficiario()
				.comNomeBeneficiario(empresa.getRazaoSocial())
				.comDocumento(insereMascaraAoRetornarDocumento(empresa.getCnpj()))
				.comNossoNumero(fatura.getNossoNumero())
				.comAgencia(conta.getAgencia())
				.comDigitoAgencia(conta.getDigitoAgencia())
				.comCodigoBeneficiario(conta.getConta())
				.comDigitoCodigoBeneficiario(conta.getDigitoConta())
				.comNumeroConvenio(fatura.getConvenio().getNumeroContrato())
				.comCarteira(fatura.getConvenio().getCarteira())
				.comEndereco(endereco);
		
		return beneficiario;
	}
	
	default String insereMascaraAoRetornarDocumento(String documento) {
		try {
			MaskFormatter mask = new MaskFormatter();
			mask.setValueContainsLiteralCharacters(false);
			if (documento.length() == 11) {
				mask.setMask("###.###.###-##");
			} else if(documento.length() == 8) {
				mask.setMask("##.###-###");
			} else {
				mask.setMask("###.###.###/####-##");
			}
			return mask.valueToString(documento);
		} catch (ParseException e) {
			throw new RuntimeException("Erro ao formatar documento.");
		}
	}
	
}
