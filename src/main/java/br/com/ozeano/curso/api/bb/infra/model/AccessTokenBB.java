package br.com.ozeano.curso.api.bb.infra.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccessTokenBB {

	private String access_token;
	private String token_type;
	private Integer expires_in;

}
